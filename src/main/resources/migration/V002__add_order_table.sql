CREATE TABLE IF NOT EXISTS shop_order (
    id bigint primary key AUTO_INCREMENT,
    buy_number INT not null ,
    commodity_id bigint not null ,
    commodity_name varchar(255) not null ,
    commodity_price INT not null ,
    commodity_unit varchar(255) not null ,
    create_time timestamp not null DEFAULT CURRENT_TIMESTAMP
)