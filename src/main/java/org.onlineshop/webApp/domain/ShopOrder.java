package org.onlineshop.webApp.domain;

import javax.persistence.*;

@SuppressWarnings("unused")
@Entity
public class ShopOrder {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(insertable = false)
    private Long id;

    @Column
    private Long commodityId;

    @Column
    private String commodityName;

    @Column
    private Integer commodityPrice;

    @Column
    private String commodityUnit;

    @Column
    private Integer buyNumber;

    public ShopOrder() {
    }

    public ShopOrder(Long commodityId,
                     String commodityName,
                     Integer commodityPrice,
                     String commodityUnit,
                     Integer buyNumber) {
        this.commodityId = commodityId;
        this.commodityName = commodityName;
        this.commodityPrice = commodityPrice;
        this.commodityUnit = commodityUnit;
        this.buyNumber = buyNumber;
    }

    public Long getId() {
        return id;
    }

    public Long getCommodityId() {
        return commodityId;
    }

    public String getCommodityName() {
        return commodityName;
    }

    public Integer getCommodityPrice() {
        return commodityPrice;
    }

    public String getCommodityUnit() {
        return commodityUnit;
    }

    public Integer getBuyNumber() {
        return buyNumber;
    }
}
